using Health.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Health.Api.Data {
    public class MedicalHistoryContext : DbContext {

        public DbSet<MedicalPatient> Patient { get; set; }

        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder) {
            //Todo: extract to config
            optionsBuilder.UseMySQL ("server=localhost;database=medicalhistory;user=user;password=password");
        }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            base.OnModelCreating (modelBuilder);

            modelBuilder.Entity<MedicalPatient> (entity => {
                entity.HasKey (e => e.Id);
                entity.Property (e => e.Name).IsRequired ();
                entity.Property (e => e.Email).IsRequired ();
                entity.Property (e => e.AvatarUrl);
            });
        }
    }
}