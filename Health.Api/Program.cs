using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Health.Api.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Health.Api {
    public class Program {
        public static void Main (string[] args) {
            CreateHostBuilder (args).Build ().Run ();
            InsertData ();
        }

        private static void InsertData () {
            using (var context = new MedicalHistoryContext ()) {
                // Creates the database if not exists
                context.Database.EnsureCreated ();
                // TODO: Seed database with some records...
                context.SaveChanges();
            }
        }

        public static IHostBuilder CreateHostBuilder (string[] args) =>
            Host.CreateDefaultBuilder (args)
            .ConfigureWebHostDefaults (webBuilder => {
                webBuilder.UseStartup<Startup> ();
            });
    }
}