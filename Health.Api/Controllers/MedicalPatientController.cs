using System.Collections.Generic;
using System.Linq;
using Health.Api.Models;
using Health.Api.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Health.Api.Controllers {
    [Authorize]
    [ApiController]
    [Route ("[controller]")]

    public class MedicalPatientController : ControllerBase {
        private readonly IMedicalPatientService patientService;

        public MedicalPatientController (IMedicalPatientService patientService) {
            this.patientService = patientService;

        }

        [AllowAnonymous]
        [HttpGet("GetAll")]
        public IActionResult Get () {
            return Ok (this.patientService.GetAllMedicalPatients ());
        }

        [HttpGet ("{id}")]
        public IActionResult GetSingle (int id) {
            return Ok (this.patientService.GetMedicalPatientById (id));
        }

        [HttpPost]
        public IActionResult AddPatient (MedicalPatient patient) {
            return Ok (this.patientService.AddMedicalPatient (patient));
        }

        //TODO: 1. make everything async
        //TODO: 2. Add PUT and DELETE actions
        //TODO: 3. Add Result logic (OK, NotFound, InternalServerError etc..)
        //TODO: 4. Add unit tests

    }

}