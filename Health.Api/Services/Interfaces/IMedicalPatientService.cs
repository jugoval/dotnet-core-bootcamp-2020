using System.Collections.Generic;
using Health.Api.Models;

namespace Health.Api.Services.Interfaces {
    public interface IMedicalPatientService {
        IEnumerable<MedicalPatient> GetAllMedicalPatients ();
        MedicalPatient GetMedicalPatientById (int id);
        IEnumerable<MedicalPatient> AddMedicalPatient (MedicalPatient patient);
    }
}