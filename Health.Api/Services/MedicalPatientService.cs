using System.Collections.Generic;
using System.Linq;
using Health.Api.Models;
using Health.Api.Services.Interfaces;

namespace Health.Api.Services {
    public class MedicalPatientService : IMedicalPatientService {
        public IEnumerable<MedicalPatient> AddMedicalPatient (MedicalPatient patient) {
            if (patient is null)
            {
                throw new System.ArgumentNullException(nameof(patient));
            }
            // Todo: Add other checks....
            patients.Add(patient);
            return patients;
        }

        public IEnumerable<MedicalPatient> GetAllMedicalPatients () {
            return patients;
        }

        public MedicalPatient GetMedicalPatientById (int id) {
            return patients.FirstOrDefault(c => c.Id == id);
        }

        private static List<MedicalPatient> patients = new List<MedicalPatient> {
            new MedicalPatient { Id = 1, Name = "Juan", Email = "juan@hotmail.com", AvatarUrl = "https://gravatar.com/avatar/8d0a9a2c8b40befb4025e67701621bdd?s=400&d=robohash&r=x" },
            new MedicalPatient { Id = 2, Name = "Andres", Email = "Andres@hotmail.com", AvatarUrl = "https://gravatar.com/avatar/33949e9e0a19cb9548b8471a901b3cda?s=400&d=robohash&r=x" },
            new MedicalPatient { Id = 3, Name = "Sergio", Email = "Sergio@hotmail.com", AvatarUrl = "https://gravatar.com/avatar/c57368cebbb5198e819b4ddf53b6199b?s=400&d=robohash&r=x" },
            new MedicalPatient { Id = 4, Name = "Camilo", Email = "Camilo@hotmail.com", AvatarUrl = "https://gravatar.com/avatar/8e918280a710dd6feeb77f2e0595947d?s=400&d=robohash&r=x" },
            new MedicalPatient { Id = 5, Name = "Johan", Email = "Johan@hotmail.com", AvatarUrl = "https://gravatar.com/avatar/218a595e58165ffbb7a3bd82f9669dfe?s=400&d=robohash&r=x" },
        };
    }
}