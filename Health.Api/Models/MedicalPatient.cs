namespace Health.Api.Models
{
    public class MedicalPatient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }   
        public string AvatarUrl { get; set; }
    }
}